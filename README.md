## Steps

1. add this project to a gitlab premium instance/cloud
1. share this project to 2 users or groups
1. ensure that `CODEOWNERS` approvals is enabled and enforced on the `main` branch   
    (Settings -> Repository -> Protected branches -> Protect the `main` branch and check `Require approval from code owners`.)
1. change the `/.gitlab/CODEOWNERS` to match 2 users or groups that have access to this repo
1. make a new branch in this repo
1. in the new branch, edit both `/CASE-A/tst-abc/test.py` and `/CASE-B/tst-xyz/test.py` so they are different from the original copies
1. make an MR from your new branch to the main branch
1. observe the codeowners approval section in the MR, it should be correct now
1. make another edit to your new branch to the `test.py` files, thus updating the MR
1. observe that the codeowners approval section is now incorrect and shows the same group/users for both `CASE-A 1` and `CASE-A 2`


## Business Usecase

2 different sections in a CODEOWNERS represents 2 different organizational units that need to approve something, perhaps QA is `CASE-A 1` and IT is `CASE-A 2`. Nobody gets it right the first time, so there's almost always additional commits to an MR.


 